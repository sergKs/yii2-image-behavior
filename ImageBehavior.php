<?php

namespace sergks\image;

use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * ImageBehavior выполняет загрузку изображений.
 * Работает в паре со стандартным валидатором.
 *
 * Пример:
 * ```php
 * public function rules()
 * {
 *     return [
 *         [['image'], 'image']
 *     ];
 * }
 *
 * public function behaviors()
 * {
 *     return [
 *         [
 *             'class' => sergks\image\ImageBehaviors::class,
 *             'attributes' => ['image']
 *         ]
 *     ];
 * }
 * ```
 *
 * @author sergKs <serg31ks@yandex.ru>
 */
class ImageBehavior extends Behavior
{
	/**
	 * @var array атрибуты
	 */
	public $attributes = [];

	/**
	 * @var string путь для загрузки изображений
	 */
	public $uploadsPath = 'uploads/img/';

	/**
	 * {@inheritDoc}
	 */
	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_INSERT => 'upload',
			ActiveRecord::EVENT_BEFORE_UPDATE => 'upload'
		];
	}

	/**
	 * Загружает изображения по указанным атрибутам [[$this->attributes]].
	 */
	public function upload()
	{
		foreach ($this->attributes as $attribute) {
			$this->uploadImage($attribute);
		}
	}

	/**
	 * Загружает изображение и сохраняет путь в указанный атрибут [[$attribute]].
	 * @param string $attribute имя атрибута
	 */
	protected function uploadImage($attribute)
	{
		/** @var $model ActiveRecord */
		$model = $this->owner;

		$file = UploadedFile::getInstance($model, $attribute);
		if ($file !== null) {
			$this->deleteOldImage($attribute);
			$model->{$attribute} = $this->createPath($file->name);
			if ($model->validate()) {
				$box = new Box(1600, 1600);
				$image = Image::getImagine()->open($file->tempName);
				$image = $image->thumbnail($box, ManipulatorInterface::THUMBNAIL_INSET);
				$image->save($this->uploadsPath . $model->{$attribute});
			}
		} else {
			if (!$model->isNewRecord) {
				$model->{$attribute} = $model->oldAttributes[$attribute];
			}
		}
	}

	/**
	 * Удаляет изображение.
	 * @param string $attribute имя атрибута
	 */
	protected function deleteImage($attribute)
	{
		/** @var $model ActiveRecord */
		$model = $this->owner;

		if (strlen(trim($model->{$attribute})) > 0) {
			if (file_exists($this->uploadsPath . $model->{$attribute})) {
				unlink($this->uploadsPath . $model->{$attribute});
				$model->{$attribute} = null;
			}
		}
	}

	/**
	 * Удаляет прошлое изображение.
	 * @param string $attribute имя атрибута
	 */
	private function deleteOldImage($attribute)
	{
		/** @var $model ActiveRecord */
		$model = $this->owner;

		if (isset($model->oldAttributes[$attribute]) && strlen($model->oldAttributes[$attribute]) > 0) {
			if (file_exists($this->uploadsPath . $model->oldAttributes[$attribute])) {
				unlink($this->uploadsPath . $model->oldAttributes[$attribute]);
			}
		}
	}

	/**
	 * Генерирует имя файла.
	 * @param $filename
	 * @return string
	 */
	public function createPath($filename)
	{
		$dirDate = date('Y/m/d/');
		$arr = explode('.', $filename);
		$ext = $arr[count($arr) - 1];

		FileHelper::createDirectory($this->uploadsPath . $dirDate);
		return $dirDate . md5(time() . $filename) . '.' . mb_strtolower($ext);
	}
}
