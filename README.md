Yii2 image behavior
-------

Расширение для загрузки изображений. Работает в паре со стандартным валидатором.

## Установка
Выполнить команду в консоли
```bash
composer require --prefer-dist sergks/yii2-image-behavior
```

или добавить в composer.json
```php
"require": {
    "sergks/yii2-image-behavior": "@dev"
}
```

## Использование

```php
/**
 * {@inheritdoc}
 */
public function rules()
{
    return [
        [['image'], 'image']
    ];
}

/**
 * {@inheritdoc}
 */
public function behaviors()
{
    return [
        [
            'class' => sergks\image\ImageBehavior::class,
            'attributes' => ['image']
        ]
    ];
}
```
